# doom-stream

A simple project to build a flatpak that runs
[freedoom](https://freedoom.github.io/) with
[chocolate-doom](https://www.chocolate-doom.org/wiki/index.php/Chocolate_Doom).

#! /bin/sh

set -e

flatpak build-export repo doom-source stable
flatpak --user remote-add --no-gpg-verify doom-repo repo
flatpak --user install doom-repo org.gnu.Doom//stable

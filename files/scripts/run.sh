#!/bin/sh

set -e

WAD="${WAD:-/app/freedoom/freedoom1.wad}"

chocolate-doom -iwad "$WAD"
